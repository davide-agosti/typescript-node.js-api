# TypeScript-Node.js-API

## Requirements

- Typescript
- NodeJS
- Express
- MongoDB

## TypeScript Node.js API project

TypeScript-Node.js-API uses Typescript, Node.js, Express and MongoDB.

## Description

This applications demonstrates how to setup a RESTful API using TypeScript with NodeJS and Express and mongoDb to create a REST. The application features a User authentication.

Running Locally

Make sure you have Node.js and gulp installed.

## Installation
- Install gulp using
```
$ npm install -g gulp
```

Use nodemon to have your server restart 
on file changes. Install nodemon using 
```
$ npm install -g nodemon
```
 
- Then start your server with nodemon dist/server.js
```
$ npm i
$ gulp build
$ npm start
```
Your app should now be running on localhost:5000.

## Configuration

Create **.env** file in root directory 
using **.env.example** file.

## Run Test.

- Run test using
```
$ npm test
```
