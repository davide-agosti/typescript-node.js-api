import { Document, Model, model, Schema } from 'mongoose';
import * as bcrypt from 'bcryptjs';
import { IUser, IUserModel } from '../interfaces/user';

const userSchema: Schema = new Schema({
  fullName: {
    firstname: {
      type: String,
      required: true
    },
    lastname: {
      type: String,
      required: true
    }
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  createAt: {
    type: Date,
    default: Date.now()
  },
  updatedAt: {
    type: Date,
    default: Date.now()
  }
});

userSchema.static('createUser', (user: IUser, callback: Function) => {
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) throw err;
      user.password = hash;
      user.save(callback());
    });
  });
});

userSchema.static('comparePassword', (candidatePassword: string, hash: string, callback: Function) => {
  bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
    if (err) throw err;
    callback(null, isMatch);
  });
});

userSchema.static('findByEmail', (email: string, callback: Function) => {
  User.findOne({ email }, callback);
});

export type UserModel = Model<IUser> & IUserModel & IUser;

export const User: UserModel = model<IUser>('User', userSchema) as UserModel;
