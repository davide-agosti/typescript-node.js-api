import { Document, Model, model, Schema } from 'mongoose';
import { User } from './user';
import { IProfile, IProfileModel } from '../interfaces/profile';

const profileSchema: Schema = new Schema({
  address: {
    street: String,
    city: String,
    postcode: String,
    country: String
  },
  phone: {
    type: String,
    required: false
  },
  fileImage: {
    type: String,
    required: false
  },
  DOB: {
    type: String,
    required: false
  },
  bio: {
    type: String,
    required: false
  },
  position: {
    type: String,
    required: false
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  user: [
    {
      type: Schema.Types.ObjectId,
      ref: 'User'
    }
  ]
});

profileSchema.static('createProfile', (profile: IProfile, callback: Function) => {
  profile.save(callback());
});

profileSchema.static('findByUser', (user: Schema.Types.ObjectId, callback: Function) => {
  Profile.findOne({ user }, callback);
});

export type ProfileModel = Model<IProfile> & IProfileModel & IProfile;

export const Profile: ProfileModel = model<IProfile>('Profile', profileSchema) as ProfileModel;
