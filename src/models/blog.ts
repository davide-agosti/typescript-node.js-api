import { Document, Model, model, Schema } from 'mongoose';
import { IBlog, IBlogModel } from '../interfaces/blog';
import { User } from './user';

const postSchema: Schema = new Schema({
  title: {
    type: String,
    required: true
  },
  slug: {
    type: String,
    required: true,
    unique: true,
    lowercase: true
  },
  fileImage: {
    type: String,
    required: false
  },
  content: {
    type: String,
    required: true
  },
  createdBy: {
    type: String,
    required: false
  },
  createdAt: {
    type: Date,
    default: Date.now()
  },
  userId: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  }
});

postSchema.static('createPost', (blog: IBlog, callback: Function) => {
  blog.save(callback());
});

postSchema.static('findBySlug', (slug: string, callback: Function) => {
  Blog.findOne({ slug }, callback);
});

postSchema.static('findById', (userId: string, callback: Function) => {
  Blog.findOne({ userId }, callback);
});

export type BlogModel = Model<IBlog> & IBlogModel & IBlog;

export const Blog: BlogModel = model<IBlog>('Blog', postSchema) as BlogModel;
