/**
 * post router
 */
import * as express from 'express';
import { Blog } from './Blog';

const blog = express.Router();

blog.use('/post', new Blog().getRoutes());

export default blog;