import * as jwt from 'jsonwebtoken';
import { Router, Request, Response } from 'express';
import { BaseRoute } from '../BaseRoute';
import { upload } from '../../utils/uploadfile';
import { User } from '../../models/user';
import {
  createPostCtrl,
  readPostCtrl,
  updatePostCtrl,
  getPostBySlugCtrl,
  deletePostCtrl
} from '../../controllers/blog';

export class Blog extends BaseRoute {
  // Get Post
  public postAction(router: Router): void {
    router.get('/showpost', readPostCtrl);
  }

  // Create Post
  public createPostAction(router: Router): void {
    router.post('/newpost', upload, this.guard, createPostCtrl);
  }

  // Get Post by slug
  public getPostBySlugAction(router: Router): void {
    router.get('/showpost/:slug', getPostBySlugCtrl);
  }

  // Update post
  public updatePostAction(router: Router): void {
    router.put('/editpost/:slug', upload, this.guard, updatePostCtrl);
  }

  // delete post
  public deletePostAction(router: Router): void {
    router.delete('/delpost/:slug', this.guard, deletePostCtrl);
  }
}
