/**
 * application main router
 */
import * as express from 'express';
import { default as userRouter } from './user';
import { default as blogRouter } from './blog';

const api = express.Router();

api.use('/user', userRouter);
api.use('/blog', blogRouter);

export default api;