import * as jwt from 'jsonwebtoken';
import { Router, Request, Response } from 'express';
import { Profile } from '../../models/profile';
import { User } from '../../models/user';
import { BaseRoute } from '../BaseRoute';
import { upload } from '../../utils/uploadfile';
import { loginCtrl, registerCtrl } from '../../controllers/authentication';
import {
  createProfileCtrl,
  readProfileCtrl,
  updateProfileCtrl
} from '../../controllers/profile';

export class Auth extends BaseRoute {
  // Login
  public loginAction(router: Router): void {
    router.post('/login', loginCtrl);
  }

  // Register User
  public registerAction(router: Router): void {
    router.post('/register', registerCtrl);
  }

  // Create profile by User
  public createProfileAction(router: Router): void {
    router.post('/createprofile', upload, this.guard, createProfileCtrl);
  }

  // Get Profile
  public profileAction(router: Router): void {
    router.get('/profile', this.guard, readProfileCtrl);
  }

  // Update profile
  public updateProfileAction(router: Router): void {
    router.put('/profile/:_id', upload, this.guard, updateProfileCtrl);
  }
}
