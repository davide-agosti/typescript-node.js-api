import * as jwt from 'jsonwebtoken';
import { Router, Request, Response } from 'express';
import { User } from '../models/user';
import { Profile } from '../models/profile';

// Create new profile
const createProfileCtrl = (req: Request, res: Response) => {
  let address = {
      street: req.body.street,
      city: req.body.city,
      postcode: req.body.postcode,
      country: req.body.country
    },
    phone = req.body.phone,
    fileImage = req.file.path,
    DOB = req.body.DOB,
    bio = req.body.bio,
    position = req.body.position,
    user = req.body.user._id;

  if (!user) {
    res.status(401).json({
      message: 'UnauthorizedError: You are not Logged In'
    });
  } else {
    // New property for Profile object
    const profile = new Profile({
      address: address,
      phone: phone,
      fileImage: fileImage,
      DOB: DOB,
      bio: bio,
      position: position,
      user: user
    });

    // Create a Profile for the user Logged In
    Profile.createProfile(profile, (err, user) => {
      if (err) {
        this.logger.error(err.toString());
        res.status(500);
        res.json({
          success: false,
          message: 'something went wrong.'
        });
      } else {
        res.json({
          success: true,
          message: 'Profile Saved.'
        });
      }
    });
  }
};

// Display Profile by user Id
const readProfileCtrl = (req: Request, res: Response) => {
  if (!req.body.user._id) {
    res.status(401).json({
      message: 'Private profile'
    });
  } else {
    let query = Profile.find({ user: req.body.user._id }).populate('user');
    query.exec((err, profile) => {
      res.status(200).json(profile);
    });
  }
};

// Update profile by Id
const updateProfileCtrl = (req: Request, res: Response) => {
  const Id = req.params._id;

  let _profile = {
    address: {
      street: req.body.street,
      city: req.body.city,
      postcode: req.body.postcode,
      country: req.body.country
    },
    phone: req.body.phone,
    fileImage: req.file.path,
    DOB: req.body.DOB,
    bio: req.body.bio,
    position: req.body.position,
    user: req.body.user._id
  };

  Profile.findOneAndUpdate({ _id: Id }, _profile, (err, profile) => {
    if (err) {
      res.status(500).json({
        message: err
      });
    }
    if (!_profile.user) {
      res.status(404).json({
        success: false,
        message: 'Profile not found, Or this profile is Private!'
      });
    } else {
      res.status(200).json({
        success: true,
        message: 'Profile updated'
      });
    }
  });
};

export { createProfileCtrl, readProfileCtrl, updateProfileCtrl };
