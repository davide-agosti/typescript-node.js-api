import * as jwt from 'jsonwebtoken';
import { Router, Request, Response } from 'express';
import { User } from '../models/user';

const loginCtrl = (req: Request, res: Response) => {
    let email = req.body.email;

    const re = /\S+@\S+\.\S+/;

    if (!re.test(email)) {
      res.status(400);
      res.json({
        success: false,
        message: 'wrong input.'
      });
      return false;
    }

    User.findByEmail(email, (err, user) => {
      if (user) {
        User.comparePassword(
          req.body.password,
          user.password,
          (err, isMatch) => {
            if (err) {
              this.logger.error(err.toString());
              res.status(500);
              res.json({
                success: false,
                message: 'something went wrong.'
              });
            } else if (isMatch) {
              const token = jwt.sign(
                { _doc: user },
                process.env.APPLICATION_SECRET,
                {
                  expiresIn: 604800 // 1 week
                }
              );

              res.json({
                success: true,
                token: token
              });
            } else {
              res.status(400);
              res.json({
                success: false,
                message: 'Wrong password.'
              });
            }
          }
        );
      } else {
        res.status(400);
        res.json({
          success: false,
          message: 'Wrong email/username.'
        });
      }
    });
  };

const registerCtrl = (req: Request, res: Response) => {
    const re = /\S+@\S+\.\S+/;

    let fullName = {
        firstname: req.body.firstname,
        lastname: req.body.lastname
      },
      email = req.body.email,
      password = req.body.password;

    if (!fullName || !re.test(email) || !password || password.length < 6) {
      res.status(400);
      res.json({
        success: false,
        message: 'Is not a valid email.'
      });
      return false;
    }

    User.findByEmail(email, (err, user) => {
      if (err) {
        this.logger.error(err.toString());
        res.status(500);
        res.json({
          success: false,
          message: 'something went wrong.'
        });
      } else if (!user) {
        const user = new User({
          fullName: fullName,
          email: email,
          password: password
        });

        User.createUser(user, (err, user) => {
          if (err) {
            this.logger.error(err.toString());
            res.status(500);
            res.json({
              success: false,
              message: 'something went wrong.'
            });
          } else {
            res.json({
              success: true,
              message: 'user created.'
            });
          }
        });
      } else {
        res.status(400);
        res.json({
          success: false,
          message: 'this email address has already been taken.'
        });
      }
    });
  };

export { loginCtrl, registerCtrl };
