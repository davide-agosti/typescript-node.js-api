import * as jwt from 'jsonwebtoken';
import { Router, Request, Response } from 'express';
import { User } from '../models/user';
import { Blog } from '../models/blog';

// Create new post
const createPostCtrl = (req: Request, res: Response) => {
  const re = /^[a-z]+(-[a-z]+)*$/;

  let title = req.body.title,
      slug = req.body.slug,
      fileImage = req.file.path,
      content = req.body.content,
      createdBy = req.body.user.name,
      userId = req.body.user._id;

  if (!re.test(slug)) {
    res.status(400);
    res.json({
      success: false,
      message: 'The slug are not in lowercase or is not separated by hyphen.'
    });
    return false;
  }

  if (!userId) {
    res.status(401).json({
      message : 'You are not Logged In'
    });
  } else { 
    // New property for Post object
    const blog = new Blog({
      title: title,
      slug: slug,
      fileImage: fileImage,
      content: content,
      createdBy: createdBy,
      userId: userId
    });

    // Create a Post from the user that Logged In
    Blog.createPost(blog, (err, blog) => {
      if (err) {
        this.logger.error(err.toString());
        res.status(500);
        res.json({
          success: false,
          message: 'something went wrong.'
        });
      } else {
        res.json({
          success: true,
          message: 'Post Saved.'
        });
      }
    });
  }

};

// Display all post saved in the database
const readPostCtrl = (req: Request, res: Response) => {
    
  Blog.find({}, (err, blog) => {
    // Check if error was found or not
    if (err) {
      res.json({
        success: false,
        message: err 
      }); // Return error message
    } else {
      if (!blog) {
        res.status(404)
        .json({
          success: false,
          message: 'No post founded.'
        });
      } else {
        res.status(200)
        .json({
          success: true,
          blog: blog
        });
      }
    }
  }).sort({ _id: -1 });

};

// Display the post by slug
const getPostBySlugCtrl = (req: Request, res: Response) => {
  const slug = req.params.slug;

  Blog.findOne({slug: slug}, (err, blog) => {
    if (err) {
      // res.status(400);
      res.json({
        success: false,
        message: err
      });
    } else {
      if (!blog) {
        res.status(404)
        .json({
          success: false,
          message: 'No post founded!'
        });
      } else {
        res.status(200)
        .json({
          success: true,
          blog: blog
        });
      }
    }
  });
};

// Update post
const updatePostCtrl = (req: Request, res: Response) => {
  const _slug = req.params.slug;

  let item = {
    title: req.body.title,
    slug: req.body.slug,
    fileImage: req.file.path,
    content: req.body.content
  };

  Blog.findOneAndUpdate({slug: _slug}, item, (err, blog) => {
    if (err) {
      res.status(500)
      .json({
        message: err
      });
    }
    res.status(200)
    .json({
      success: true,
      message: 'Post updated!'
    });
  });
};

// Delete Post
const deletePostCtrl = (req: Request, res: Response) => {
  const _slug = req.params.slug;

  Blog.findOneAndRemove({slug: _slug}, (err, blog) => {
    if (err) {
      res.status(500)
      .json({
        message: err
      });
    }
    res.status(200)
    .json({
      success: true,
      message: 'Post deleted!'
    });
  });
};

export { createPostCtrl, readPostCtrl, updatePostCtrl, getPostBySlugCtrl, deletePostCtrl };
