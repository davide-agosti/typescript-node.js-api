import * as multer from 'multer';

// Set Storage Engine
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
    cb(null, './public/uploads/');
    },
    filename: (req, file, cb) => {
    cb(null, file.fieldname + '_' + Date.now() + '_' + file.originalname);
    }
});

// Init upload
const upload = multer({ 
    storage: storage
}).single('fileImage');

export { upload };
