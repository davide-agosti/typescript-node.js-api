import { Document, Model, model, Schema } from 'mongoose';

export interface IProfile extends Document {
  address: {
    street: string;
    city: string;
    postcode: string;
    country: string;
  };
  phone: string;
  fileImage: string;
  DOB: string;
  bio: string;
  position: string;
  user: string;
}

export interface IProfileModel {
  createProfile(profile: IProfile, callback: Function): void;
  findByAddress(address: string, callback: Function): void;
}
