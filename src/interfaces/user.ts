
import { Document, Model, model, Schema } from 'mongoose';

export interface IUser extends Document {
  fullName: {
    firstname: string;
    lastname: string;
  };
  email: string;
  password: string;
}

export interface IUserModel {
  createUser(user: IUser, callback: Function): void;
  comparePassword(
    candidatePassword: string,
    hash: string,
    callback: Function
  ): void;
  findByEmail(email: string, callback: Function): void;
}
