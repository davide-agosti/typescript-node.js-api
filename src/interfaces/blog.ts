import { Document, Model, model, Schema } from 'mongoose';

export interface IBlog extends Document {
  title: string;
  slug: string;
  fileImage: string;
  content: string;
  createdBy: string;
  userId: string;
}

export interface IBlogModel {
  createPost(blog: IBlog, callback: Function): void;
  updatePost(blog: IBlog, callback: Function): void;
  findBySlug(slug: string, callback: Function): void;
  findById(userId: string, callback: Function): void;
}
