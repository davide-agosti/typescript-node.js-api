const   gulp = require('gulp'),
		ts = require('gulp-typescript'),
		clean = require('gulp-clean');

const   DIST_DIRECTORY = 'dist',
		tsProject = ts.createProject('tsconfig.json');

// ** Pre-Configurations * //
gulp.task('watch', ['build-ts'], () => {
    gulp.watch('src/**/*.ts', ['build-ts']);
});

// ** Clean when false* //
gulp.task('clean-scripts', () => {
    return gulp.src(DIST_DIRECTORY, {read: false}).pipe(clean());
});

// ** Compilation ** //
gulp.task('build-ts', () => {
    const tsResult = tsProject.src().pipe(tsProject());
    return tsResult.js.pipe(gulp.dest(DIST_DIRECTORY));
});

gulp.task('build', ['build-ts']);